# a11y-menu

A menu that is fully accessible. It has no design or theme applied. That way you can be certain the menu is usable and accessible and you can apply your own styles.

[![Pipeline status](https://gitlab.com/wanjapflueger/a11y-menu/badges/master/pipeline.svg)](https://gitlab.com/wanjapflueger/a11y-menu/-/pipelines)
![npm](https://img.shields.io/npm/v/@wanjapflueger/a11y-menu)
![npm bundle size](https://img.shields.io/bundlephobia/min/@wanjapflueger/a11y-menu)

---

## Table of contents

<!-- TOC -->

- [a11y-menu](#a11y-menu)
  - [Table of contents](#table-of-contents)
  - [Requirements](#requirements)
  - [Changelog](#changelog)
  - [Install](#install)
  - [Example](#example)
  - [Usage](#usage)
    - [Init](#init)
    - [Methods](#methods)
      - [Method Create](#method-create)
        - [Parameters](#parameters)
    - [Config](#config)
    - [Styles](#styles)
      - [Selectors](#selectors)
      - [Example Styles](#example-styles)
    - [Keyboard support](#keyboard-support)
  - [Accessibility Compliance Report](#accessibility-compliance-report)

<!-- /TOC -->

---

## Requirements

- You need to be able to <a target="_blank" href="https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Statements/import">import</a> or <a target="_blank" href="https://nodejs.org/en/knowledge/getting-started/what-is-require/">require</a> _JavaScript_ files

## Changelog

See [changelog.md](https://gitlab.com/wanjapflueger/a11y-menu/-/blob/master/changelog.md).

## Install

```bash
npm i @wanjapflueger/a11y-menu
```

## Example

[example.barrierefrei.kiwi/a11y-menu/](https://example.barrierefrei.kiwi/a11y-menu/)

## Usage

### Init

Import class `Menu`.

```js
import { Menu } from '@wanjapflueger/a11y-menu';
// var a11yMenu = require("@wanjapflueger/a11y-menu")
```

Create a new instance

```js
const myMenu = new Menu();
// const myMenu = new a11yMenu.Menu();
```

### Methods

#### Method Create

1. Create a navigation of this simple structure.

   ```html
   <nav>
     <ul>
       <li><a href="/">Home</a></li>
       <li>
         <span>Animals</span>
         <ul>
           <li><a href="/animals/birds">Birds</a></li>
           <li><a href="/animals/cats">Cats</a></li>
           <li><a href="/animals/dogs">Dogs</a></li>
         </ul>
       </li>
       <li><a href="/contact">Contact</a></li>
       <li><a href="/privacy">Privacy</a></li>
     </ul>
   </nav>
   ```

   **Important:**

   - A menu item that has a submenu cannot be a link itself
     ```html
     <!-- ✅ Do -->
     <li>
       <span>Animals</span>
       <ul>
         <li><a href="/animals">All animals</a></li>
         <li><a href="/animals/cats">Cats</a></li>
         <li><a href="/animals/dogs">Dogs</a></li>
         <li>...</li>
       </ul>
     </li>

     <!-- ❌ Do not -->
     <li>
       <a href="/animals">Animals</a>
       <ul>
         <li><a href="/animals/cats">Cats</a></li>
         <li><a href="/animals/dogs">Dogs</a></li>
         <li>...</li>
       </ul>
     </li>
     ```

   - A menu item with a submenu must contain a `HTMLSpanElement` and a `HTMLUListElement`. The first `HTMLSpanElement` will be used as the label for the`HTMLUListElement`.
     ```html
     <!-- ✅ Do -->
     <li>
       <span>Label for list</span>
       <span>Other content</span>
       <ul>
         <li>...</li>
       </ul>
     </li>

     <!-- ❌ Do not -->
     <li>
       <span>Other content</span>
       <span>Label for list</span>
       <ul>
         <li>...</li>
       </ul>
     </li>
     ```

1. Assign that navigation element with _JavaScript_ to a variable, initiate a new instance of `Menu` and call _method_ `Create`.

   ```js
   import { Menu } from '@wanjapflueger/a11y-menu';

   const myMenu = new Menu();
   myMenu.Create({
     nav: document.querySelector('nav'),
   });
   ```

##### Parameters

See `MenuParameters` in [src/index.ts](https://gitlab.com/wanjapflueger/a11y-menu/-/blob/master/src/index.ts).

| Name                                         | Required | Type                | Default value | Description                                                    | Example                                                                                          |
| -------------------------------------------- | -------- | ------------------- | ------------- | -------------------------------------------------------------- | ------------------------------------------------------------------------------------------------ |
| <span id="parameters-nav"></span>`nav`       | ✅ Yes   | `HTMLElement`       |               | Navigation element. Any `HTMLElement` being a `<nav>` element. | `<nav>...</nav>`                                                                                 |
| <span id="parameters-config"></span>`config` | ❌ No    | [`Config`](#config) | `undefined`   | Menu configuration.                                            | `{ responsive: [{minWidth: 768, config: [{closeOnNavigate: true, direction: 'x', level: 0}]}] }` |

### Config

```js
import { Menu } from '@wanjapflueger/a11y-menu';

const myMenu = new Menu();
myMenu.Create({
  nav: document.querySelector('nav'),
  config: {
    breakpoints: [
      {
        minWidth: 0,
        config: [
          {
            closeOnNavigate: false,
            level: 0,
          },
        ],
      },
      {
        minWidth: 768,
        config: [
          {
            closeOnNavigate: false,
            closeOnClick: true,
            direction: 'vertical',
            level: 0,
          },
        ],
      },
      {
        minWidth: 1280,
        config: [
          {
            direction: 'horizontal',
            level: 0,
          },
          {
            direction: 'vertical',
            level: 1,
          },
        ],
      },
    ],
  },
});
```

This would mean:

1. On screens bigger than `768px` and smaller than `1280px`...

   1. on `MenuItem.level === 0` use the up- and down arrow keys to go to the next `MenuItem`

1. On screens bigger than `1280px`...

   1. On `MenuItem.level === 0` use the left- and right keys to go to the next `MenuItem`

   1. On `MenuItem.level === 1` use the up- and down keys to go to the next `MenuItem`

The default value for `direction` is always `'vertical'`.

### Styles

The menu does not come with any design or theme applied.

#### Selectors

To apply styles for different states, use these selectors:

```css
[role='menubar'] {
  /** Top level menu */
}

[role='menu'] {
  /** Sub menus */
}

[role='none'] {
  /** Menu item parent with no function. Only when the [role='menuitem'] is an anchorlink element */
}

a[role='menuitem'] {
  /** Menu item that is clickable */
}

li[role='menuitem'] {
  /** Menu item that contains a submenu ([role="menu"]) */
}

[role='menuitem'][aria-expanded='true'] {
  /** Menu items that are expanded */
}

[role='menuitem'][data-focus='true'] {
  /** The menu item that has the focus */
}

[role='menuitem'][aria-current='page'] {
  /** Menu items that match the current page */
}

[role='menuitem'][data-current-parent='true'] {
  /** Menu items that are parents of the [aria-current="page"] */
}

[role='menuitem'][data-level='{number}'] {
  /** The depth level of a menu item (replace `{number}` with a number, starting with `0`) */
}
```

#### Example Styles

Here are some [example styles](#example-styles).

```css
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}

[role='menubar'] {
  display: flex;
}

[role='menuitem'] {
  position: relative;
  background-color: #ccc;
  min-width: 100px;
  display: block;
  cursor: pointer;
  padding: 5px 10px;
}

[role='menuitem']:hover,
[role='menuitem']:focus-visible {
  text-decoration: underline;
}

[role='menuitem'] > ul {
  position: absolute;
}

[aria-expanded='false']::before {
  content: '→';
}
[aria-expanded='true']::before {
  content: '↓';
}

[aria-expanded='false'] > ul {
  display: none;
}

nav > ul > li > ul > li ul {
  left: 100%;
  top: 0;
  outline: 1px solid red;
}
```

### Keyboard support

In [index.ts at menuItemControls](https://gitlab.com/wanjapflueger/a11y-menu/-/blob/master/src/index.ts) several keys have been assigned a function. The keyboard support starts when any element in the menu is active. A menu is active when:

- any `nav [role="menuitem"]` is focussed

| Key          | Description                                       |
| ------------ | ------------------------------------------------- |
| `ArrowRight` | Next or submenu (`direction === 'horizontal'`)    |
| `ArrowDown`  | Next or submenu (`direction === 'vertical'`)      |
| `ArrowLeft`  | Previous or parent (`direction === 'horizontal'`) |
| `ArrowUp`    | Previous or parent (`direction === 'vertical'`)   |

## Accessibility Compliance Report

[WCAG](https://www.w3.org/WAI/WCAG21/quickref/) Level: **AAA** [^1]

| Browser           | Platform      | Screen reader                                                                | Passed |
| ----------------- | ------------- | ---------------------------------------------------------------------------- | ------ |
| Chrome 95         | Windows 10    | [NVDA](https://www.nvaccess.org/)                                            | ✅     |
| Firefox 93        | Windows 10    | [NVDA](https://www.nvaccess.org/)                                            | ✅     |
| Microsoft Edge 95 | Windows 10    | [NVDA](https://www.nvaccess.org/)                                            | ✅     |
| Chrome 94         | Android 10    | [TalkBack](https://www.samsung.com/uk/accessibility/mobile-voice-assistant/) | ✅     |
| Chrome 95         | MacOS 11.15.2 | [VoiceOver](https://www.apple.com/accessibility/vision/)                     | ✅     |
| Firefox 90        | MacOS 11.15.2 | [VoiceOver](https://www.apple.com/accessibility/vision/)                     | ✅     |
| Microsoft Edge 95 | MacOS 11.15.2 | [VoiceOver](https://www.apple.com/accessibility/vision/)                     | ✅     |

---

[^1]: This information refers only to the technical aspects of the component, not to the design or the editorial handling of any content.
