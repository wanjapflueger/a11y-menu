/**
 * Listen for clicks on the `document` element, that are outside of {@link el} and call {@link callbackFunction}
 */
export const onClickOutside = (
  el: HTMLElement,
  callbackFunction: () => void,
) => {
  document.addEventListener(
    'click',
    (e: MouseEvent) => {
      if (!el.contains(e.target as Element)) callbackFunction.call({});
    },
    true,
  );
};

/**
 * Create a random string from a set of characters with a specific length
 * @param length Any number
 * @param charset Character set used to create id
 * @param pre Text to prepend
 * @returns Random string with characters from {@link charset} with the length of {@link length}
 */
export const makeId = (
  length: number,
  charset: string = 'abcdefghijklmnopqrstuvwxyz',
  pre?: string,
) => {
  let result = '';
  if (pre) result += pre;
  for (let i = 0; i < length; i++) {
    result += charset.charAt(Math.floor(Math.random() * charset.length));
  }
  return result;
};

/**
 * Strip a hash a string
 * @param str String with or without #
 * @returns String without # and what follows
 */
export const stripHash = (str: string) =>
  str.includes('#') ? str.split('#')[0] : str;

/**
 * Strip the trailing slash from a string
 * @param str String with or without /
 * @returns String without /
 */
export const stripTrailingSlash = (str: string) =>
  str.endsWith('/') ? str.slice(0, -1) : str;
