import {
  makeId,
  onClickOutside,
  stripHash,
  stripTrailingSlash,
} from './_helper';
import { throttle } from 'throttle-debounce';

interface MenuItem {
  /** The `<nav>` `HTMLElement` (with `nodeName === 'NAV'`) that is the wrapping element for this {@link MenuItem.el} */
  menu: HTMLElement;

  /** DOM reference */
  el: HTMLAnchorElement | HTMLLIElement;

  /** Position in menu (`0` is the first item) */
  index: number;

  /** Nesting level of {@link MenuItem.el} in {@link MenuItem.menu} starting with `0` */
  level: number;

  /** `true` if this {@link MenuItem} is the first {@link MenuItem} in the current context (submenu) */
  isFirst: boolean;

  /** `true` if this {@link MenuItem} is the last {@link MenuItem} in the current context (submenu) */
  isLast: boolean;

  /** `true` if this {@link MenuItem} is the currently active {@link MenuItem} */
  current: boolean;

  /** The previous focussable {@link MenuItem} in the current context (submenu) */
  previous?: MenuItem;

  /** The next focussable {@link MenuItem} in the current context (submenu) */
  next?: MenuItem;

  /** If this {@link MenuItem} has a submenu, then {@link MenuItem.subMenuItem} is the first focussable {@link MenuItem} in the submenu */
  subMenuItem?: MenuItem;

  /** If this {@link MenuItem} is part of a submenu, then {@link MenuItem.parentMenuItem} is the {@link MenuItem}, that triggered the submenu */
  parentMenuItem?: MenuItem;

  /** If possible, focus on {@link MenuItem.next}. Returns `true` if possible. */
  goToNextItem(): boolean;

  /** If possible, focus on {@link MenuItem.previous}. Returns `true` if possible. */
  goToPreviousItem(): boolean;

  /** If possible, go to {@link MenuItem.subMenuItem}. Returns `true` if possible. */
  goToNextLevel(): boolean;

  /** If possible, go to {@link MenuItem.parentMenuItem}. Returns `true` if possible. */
  goToPreviousLevel(): boolean;

  /** If possible, go to the next {@link MenuItem} on {@link MenuItem.level} `0`. Returns `true` if possible. */
  goToNextRootItem(): boolean;
}

/**
 * The configuration for a {@link MenuBreakpoint}
 */
interface MenuBreakpointConfig {
  /** A {@link MenuItem.level} value (min: `0`) for the current configuration */
  level: number;

  /**
   * To get to the previous and next {@link MenuItem} of the same {@link MenuItem.level}...
   * - case `"horizontal"` -> use `ArrowLeft | ArrowRight`.
   * - case `"vertical"` -> use `ArrowUp | ArrowDown`.
   */
  direction: 'horizontal' | 'vertical';

  /** Close all {@link MenuItem}s in a {@link MenuItem.level} when opening on another {@link MenuItem} of the same {@link MenuItem.level}. Default is `true`. */
  closeOnNavigate?: boolean;

  /** Close all {@link MenuItem}s when clicking a {@link MenuItem} with a hyper link reference (link). Default is `false`. */
  closeOnClick?: boolean;
}

/**
 * Settings that apply relative to a specific breakpoint
 */
interface MenuBreakpoint {
  /** Represents a minimum `document.body.clientWidth` value */
  width: number;

  /** The configuration for a `document.body.clientWidth` >= {@link width} */
  config?: MenuBreakpointConfig[];
}

/**
 * General settings
 */
export interface MenuConfig {
  /** Responsive configuration (make sure each {@link MenuBreakpoint} has a unique {@link MenuBreakpoint.width}) */
  breakpoints?: MenuBreakpoint[];
}

/**
 * Menu Parameters
 */
interface MenuParameters {
  /** Any `<nav>` `HTMLElement` (with `nodeName === 'NAV'`) */
  nav: HTMLElement;

  /** Responsive Settings */
  config?: MenuConfig;
}

/**
 * A11Y Menu. A fully accessible menu.
 * @author Wanja Friedemann Pflüger <noreply@wanjapflueger.de>
 * @example
 *   import { Menu } from "@wanjapflueger/a11y-menu";
 *   const myMenu = new Menu();
 */
export class Menu {
  /** Parameters */
  parameters: MenuParameters | undefined;

  /**
   * The menu element
   * @example
   *   ```html
   *   <nav>...</nav>
   *   ```
   */
  theMenu: HTMLElement | undefined;

  /** All focussable elements within nav (will be `HTMLLIElement`s that have submenus and `HTMLAnchorElement`s) */
  focussableElements: HTMLElement[];

  /** All {@link MenuItem}s in {@link theMenu} */
  menuItems: MenuItem[];

  /** The default value for {@link MenuBreakpointConfig.direction} */
  private defaultDirection: MenuBreakpointConfig['direction'];

  /** The default value for {@link MenuConfig} */
  private defaultConfig: MenuConfig;

  constructor() {
    this.defaultDirection = 'vertical';
    this.menuItems = [];
    this.focussableElements = [];
    this.defaultConfig = {
      breakpoints: [
        {
          width: 1280,
          config: [
            {
              level: 0,
              direction: 'horizontal',
            },
          ],
        },
      ],
    };
  }

  /**
   * Compare any {@link MenuItem} with any URL. Return `true` if the {@link menuItem}s `href` matches that URL. Adds attribute `data-current-parent="true"` to all {@link MenuItem} ancestors. Adds `aria-current="page"` to the {@link MenuItem.el}.
   * @param url Any URL
   * @param menuItem Any menu item
   * @example
   *   ```html
   *   <a href="/my-page"></a>
   *   ```
   *   handleCurrentPage(window.location.href, menuItem)
   *   ```html
   *   <a aria-current="page" href="/my-page"></a>
   *   ```
   */
  private handleCurrentPage = (url: string, menuItem: MenuItem) => {
    const page = new URL(url);

    /** `true` if {@link menuItem} is the current page {@link MenuItem} */
    let isCurrent = false;

    if (menuItem.el.nodeName !== 'A') return;
    if (!menuItem.el.hasAttribute('href')) return;
    if (!page) return;

    const href = menuItem.el.getAttribute('href');

    if (href) {
      /** Exclude anchor links */
      if (href.startsWith('#')) return;

      if (href.startsWith('/')) {
        if (href.startsWith('//')) {
          /** Handle URLs with host and without http(s) */
          const menuItemUrl = new URL(page.protocol + href);

          if (menuItemUrl && menuItemUrl.host === page.host) {
            /** URL host matches so it is an internal URL */
            if (
              stripTrailingSlash(stripHash(menuItemUrl.pathname)) ===
              stripTrailingSlash(stripHash(page.pathname))
            ) {
              isCurrent = true;
            }
          }
        } else {
          /** Handle absolute urls */
          if (
            stripTrailingSlash(stripHash(href)) ===
            stripTrailingSlash(stripHash(page.pathname))
          ) {
            isCurrent = true;
          }
        }
      } else {
        if (href.startsWith('http')) {
          /** Handle URLs with host and with http(s) */
          const menuItemUrl = new URL(href);

          if (menuItemUrl && menuItemUrl.host === page.host) {
            /** URL host matches so it is an internal URL */
            if (
              stripTrailingSlash(stripHash(menuItemUrl.pathname)) ===
              stripTrailingSlash(stripHash(page.pathname))
            ) {
              isCurrent = true;
            }
          }
        } else {
          /** We do not need to handle relative urls */
        }
      }
    }

    if (isCurrent === true) {
      /** We found a {@link MenuItem} that is very likely the current page. We can now mark all parent items so we can apply styles to them */
      let item = menuItem;

      item.el.setAttribute('aria-current', 'page');

      while (item.parentMenuItem) {
        /** Each parent {@link MenuItem} will have a `data-current-parent="true"` attribute */
        item.parentMenuItem.el.dataset.currentParent = 'true';
        item = item.parentMenuItem;
      }
    }
  };

  /**
   * The menu elements
   * @example
   *   myMenu.elements
   *   myMenu.elements.foo
   *   ...
   */
  private get elements() {
    /** The first `<ul>` ELement in {@link theMenu} */
    const list: HTMLUListElement | null | undefined =
      this.theMenu?.querySelector(':scope > ul');

    /** All `HTMLUListElement`s that are not {@link list} */
    const uListElements: NodeListOf<HTMLUListElement> | undefined =
      this.theMenu?.querySelectorAll(':scope > ul span + ul');

    /** All `HTMLSpanElement`s that are the caption for a submenu */
    const spanElements: HTMLSpanElement[] | undefined = [];

    if (this.theMenu) {
      Array.from(this.theMenu.querySelectorAll('li')).forEach((el) => {
        const firstSpan = el.querySelector('span');
        if (firstSpan && el.querySelector('ul')) {
          spanElements.push(firstSpan);
        }
      });
    }

    /** All `HTMLLIElement`s and `HTMLAnchorElement` in {@link list} */
    const listItems: NodeListOf<HTMLLIElement | HTMLAnchorElement> | undefined =
      this.theMenu?.querySelectorAll('li, a');

    return {
      list,
      uListElements,
      spanElements,
      listItems,
    };
  }

  /**
   * Close all currently opened {@link MenuItem}s
   */
  CloseAll = () => {
    const openItems = this.focussableElements.filter(
      (x) =>
        x.getAttribute('aria-expanded') === 'true' &&
        x.getAttribute('data-focus') !== 'true',
    );

    openItems.forEach((openItem) => {
      openItem.setAttribute('aria-expanded', 'false');
    });
  };

  /**
   * Close all currently opened {@link MenuItem}s on scroll
   */
  private OnScrollCloseAll() {
    /** How many {@link update}s until we call {@link Menu.CloseAll} */
    const threshold = 10;

    /** Count the {@link update}s */
    let thresholdCounter = 0;

    const update = () => {
      if (thresholdCounter < threshold) {
        thresholdCounter++;
      } else {
        /** Reset */
        thresholdCounter = 0;

        /** Close */
        this.CloseAll();
      }
    };

    const tick = throttle(100, update);

    window.addEventListener('scroll', tick);
  }

  /**
   * Create an accessible menu. This will not generate a new `HTMLElement` but will alter attributes etc. on the exisisting `Element`.
   * @example
   *   ```html
   *   <nav>
   *     <ul>
   *       <li><a href="/">Home</a></li>
   *       <li>
   *         <span>Topics</span>
   *         <ul>
   *           <li><a href="/nature">Nature</a></li>
   *           <li><a href="/people">People</a></li>
   *         </ul>
   *       </li>
   *       <li><a href="/contact">Contact</a></li>
   *     </ul>
   *   </nav>
   *   <script>
   *     import { Menu } from "@wanjapflueger/a11y-menu";
   *     const myMenu = new Menu();
   *     myMenu.Create({
   *       nav: document.querySelector('nav')
   *     })
   *   </script>
   *   ```
   * @returns True if successfull
   */
  Create(parameters: MenuParameters): boolean {
    // apply parameters
    if (!parameters) return false;
    this.parameters = parameters;

    /** If no configuration is provided, use the default configuration */
    if (!this.parameters.config) this.parameters.config = this.defaultConfig;

    if (!this.parameters.nav)
      throw Error(`Missing required argument 'this.parameters.nav'`);

    if (this.parameters.nav.nodeName !== 'NAV')
      throw TypeError(
        `Expected argument 'this.parameters.nav' to have a nodeName of 'NAV' but was '${this.parameters.nav.nodeName}'`,
      );

    // make sure no ResponsiveConfig.minWidth value is < 0
    if (
      this.parameters.config?.breakpoints?.length &&
      this.parameters.config.breakpoints.find((item) => item.width < 0) !==
        undefined
    )
      return false;

    this.theMenu = this.parameters.nav;

    /** Find the {@link MenuBreakpointConfig} of any {@link MenuItem}  */
    const findDirectionConfig = (
      menuItem: MenuItem,
    ): undefined | MenuBreakpointConfig => {
      let theDirections: MenuBreakpointConfig[] | undefined;

      const breakpoints = this.parameters?.config?.breakpoints;
      if (!breakpoints) return undefined;

      breakpoints.forEach((breakpoint) => {
        if (breakpoint.width <= document.body.clientWidth) {
          theDirections = breakpoint.config;
        }
      });

      return theDirections?.find((el) => el.level === menuItem.level);
    };

    /**
     * Reset all {@link MenuItem}s in a {@link MenuItem.level}
     */
    const resetAllInLevel = (level?: MenuItem['level']) => {
      // only reset if anything has to be resetted
      if (
        !this.menuItems.find(
          (x) => x.el.getAttribute('aria-expanded') === 'true',
        )
      )
        return;

      this.menuItems.forEach((menuItem) => {
        if (level) {
          if (menuItem.level === level) {
            if (menuItem.el.nodeName === 'LI') {
              menuItem.el.setAttribute('aria-expanded', 'false');
            }
          }
        } else {
          if (menuItem.el.nodeName === 'LI') {
            menuItem.el.setAttribute('aria-expanded', 'false');
          }
        }
      });
    };

    /**
     * Set default `role` attribute values on all `HTMLElement`s that belong to {@link theMenu} (like `role="menubar"`, `role="menuitem"`, etc.)
     */
    const setDefaultAttributes = () => {
      /** set `role` to `menubar` on the first `HTMLUListElement` */
      this.elements.list?.setAttribute('role', 'menubar');

      /** set `role` to `menu` on all `HTMLUListElement`s */
      this.elements.uListElements?.forEach((item) => {
        item.setAttribute('role', 'menu');
      });

      /** set `role` to `none` on all `HTMLLIElement`s */
      this.elements.listItems?.forEach((item) => {
        item.setAttribute('role', 'none');

        /* find elements that are focussable (`HTMLLIElement`s with submenu or `HTMLAnchorElement`s) */
        if (item.querySelector('ul') || item.nodeName === 'A') {
          this.focussableElements?.push(item);
        }
      });

      /** use `HTMLSpanElement`s as label for `HTMLUListElement`s – given the are a submenu item */
      this.elements.spanElements?.forEach((item) => {
        const id = makeId(10, undefined, 'label-');
        item.id = id;
        const submenuItem = item.parentElement?.querySelector('ul');
        submenuItem?.setAttribute('aria-labelledby', id);
      });
    };

    const findMenuItemParameters = () => {
      /**
       * Find out the {@link MenuItem.level} of a {@link MenuItem}
       * @returns {number}
       */
      const findLevel = (menuItem: MenuItem) => {
        /** 1 = Level 1, 2 = Level 2, ... */
        let level: number = 0;

        let match: HTMLElement = menuItem.el;

        while (match) {
          match =
            match && match.nodeName === 'A'
              ? (match.parentNode?.parentNode as HTMLElement)
              : (match.parentNode as HTMLElement);

          if (match === this.elements.list) break;

          if (match?.nodeName === 'LI') {
            level++;
          }
        }

        return level;
      };

      /**
       * Find out if {@link MenuItem} is the first in its submenu
       */
      const checkIfIsFirst = (menuItem: MenuItem) =>
        menuItem.level > 0 &&
        !menuItem.el.previousElementSibling &&
        !menuItem.el.parentElement?.previousElementSibling;

      /**
       * Find out if {@link MenuItem} is the last in its submenu
       */
      const checkIfIsLast = (menuItem: MenuItem) =>
        menuItem.level > 0 &&
        !menuItem.el.nextElementSibling &&
        !menuItem.el.parentElement?.nextElementSibling;

      /**
       * Find out if a {@link MenuItem} has a submenu and find the first {@link MenuItem} in that submenu
       */
      const findSubMenuItem = (menuItem: MenuItem) => {
        if (
          !menuItem.el.querySelector('ul') &&
          !menuItem.el.parentNode?.querySelector('ul')
        )
          return;
        let match;

        const items = this.menuItems.filter(
          (el) => el.index > menuItem.index && el.level === menuItem.level + 1,
        );

        if (items && items.length) {
          match = items[0];
        }

        return match;
      };

      /**
       * Find out if a {@link MenuItem} is within a submenu and therefore has a parent menu and find the belonging {@link MenuItem}
       */
      const findParentMenuItem = (menuItem: MenuItem) => {
        /** The direct parent of {@link menuItem} */
        let match: MenuItem | undefined;

        /** All parent menu items of {@link menuItem} */
        const parentMenuItems = this.menuItems.filter((el) => {
          // Exclude items that are links
          if (el.el.nodeName !== 'LI') return;

          // Exclude items with a higher level
          if (el.level >= menuItem.level) return;

          // Exclude items with a higher index
          if (el.index >= menuItem.index) return;

          /** Find out if {@link menuItem} is actually in {@link el}. At this point we do not have `[role="menuitem"]` and have to use `'a, li'` as a selector. */
          if (
            Array.from(el.el.querySelectorAll('a, li')).find(
              (x) => x === menuItem.el,
            )
          )
            return el;
        });

        if (!parentMenuItems || !parentMenuItems.length) return;

        /** Reverse order from „grand-parent to parent” to „parent to grand parent” */
        parentMenuItems.reverse();

        /** The match is now the direct parent of the {@link menuItem} */
        match = parentMenuItems[0];

        return match;
      };

      /**
       * Find the next {@link MenuItem} that would follow the current {@link MenuItem}
       */
      const findNextMenuItem = (menuItem: MenuItem) => {
        /** if this is already the last {@link MenuItem} in the current submenu, there can be no next {@link MenuItem} */
        if (menuItem.isLast) return undefined;

        let match: MenuItem | undefined;

        match = this.menuItems.find(
          (el) => el.level === menuItem.level && el.index > menuItem.index,
        );

        if (!match) {
          const sameLevelItems = this.menuItems.filter(
            (el) => el.level === menuItem.level,
          );
          if (sameLevelItems && sameLevelItems.length) {
            match = sameLevelItems[0];
          }
        }

        return match;
      };

      /**
       * Find the previous {@link MenuItem} that would precede the current {@link MenuItem}
       */
      const findPreviousMenuItem = (menuItem: MenuItem) => {
        /** if this is already the first {@link MenuItem} in the current submenu, there can be no previous {@link MenuItem} */
        if (menuItem.isFirst) return undefined;

        let match: MenuItem | undefined;

        const menuItemsReversed = this.menuItems.slice().reverse();
        if (!menuItemsReversed || !menuItemsReversed.length) return;

        menuItemsReversed.find((el) => {
          if (!match) {
            if (el.level === menuItem.level && el.index < menuItem.index) {
              match = el;
            }
          }
        });

        if (!match) {
          const sameLevelItems = this.menuItems.filter(
            (el) => el.level === menuItem.level,
          );
          if (sameLevelItems && sameLevelItems.length) {
            match = sameLevelItems[sameLevelItems.length - 1];
          }
        }

        return match;
      };

      /**
       * Find values for each {@link MenuItem}
       */
      const findMenuItemElements = () => {
        for (let i = 0, n = this.focussableElements; i < n.length; i++) {
          const item = n[i];
          /** Define {@link MenuItem} for the first time */
          const menuItem: MenuItem = {
            el:
              item.nodeName === 'A'
                ? (item as HTMLAnchorElement)
                : (item as HTMLLIElement),
            index: -1,
            level: -1,
            isFirst: false,
            isLast: false,
            current: false,
            menu: this.theMenu as HTMLElement,
            goToPreviousItem: () => {
              if (!menuItem.current) return false;
              if (menuItem.level === 0) {
                const directionConfig = findDirectionConfig(menuItem);

                if (directionConfig?.closeOnNavigate !== false) {
                  resetAllInLevel();
                }
              }
              if (menuItem.previous) {
                menuItem.previous.el.focus();
                return true;
              }
              return false;
            },
            goToNextItem: () => {
              if (!menuItem.current) return false;
              if (menuItem.level === 0) {
                const directionConfig = findDirectionConfig(menuItem);

                if (directionConfig?.closeOnNavigate !== false) {
                  resetAllInLevel();
                }
              }
              if (menuItem.next) {
                menuItem.next.el.focus();
                return true;
              }
              return false;
            },
            goToPreviousLevel: () => {
              if (!menuItem.current) return false;
              if (menuItem.parentMenuItem) {
                if (menuItem.el.getAttribute('aria-expanded') === 'true') {
                  menuItem.el.setAttribute('aria-expanded', 'false');
                }
                menuItem.parentMenuItem.el.focus();
                return true;
              }
              return false;
            },
            goToNextLevel: () => {
              if (!menuItem.current) return false;
              if (menuItem.subMenuItem) {
                menuItem.el.setAttribute('aria-expanded', 'true');
                menuItem.subMenuItem.el.focus();
                return true;
              }
              return false;
            },
            goToNextRootItem: () => {
              if (!menuItem.current) return false;
              const currenLevel1Item = this.menuItems.find(
                (el) =>
                  el.level === 0 &&
                  el.el.getAttribute('aria-expanded') === 'true',
              );
              if (currenLevel1Item) {
                const directionConfig = findDirectionConfig(menuItem);

                if (directionConfig?.closeOnNavigate !== false) {
                  resetAllInLevel();
                }
                currenLevel1Item.next?.el.focus();
                return true;
              }
              return false;
            },
          };

          this.menuItems.push(menuItem);
        }
      };

      findMenuItemElements();

      if (!this.menuItems) return;

      this.menuItems.forEach((menuItem, i) => {
        menuItem.index = i;
        menuItem.level = findLevel(menuItem);
        menuItem.current = false;
      });

      this.menuItems.forEach((menuItem: MenuItem) => {
        if (menuItem.isLast)
          menuItem.el.dataset.last = menuItem.isLast ? 'true' : 'false';
        menuItem.parentMenuItem = findParentMenuItem(menuItem);
        menuItem.subMenuItem = findSubMenuItem(menuItem);
        menuItem.isFirst = checkIfIsFirst(menuItem);
        menuItem.isLast = checkIfIsLast(menuItem);
        menuItem.previous = findPreviousMenuItem(menuItem);
        menuItem.next = findNextMenuItem(menuItem);
      });
    };

    /**
     * Bind event listeners
     */
    const bindEventListeners = (menuItem: MenuItem) => {
      /**
       * Click handler for a {@link MenuItem}
       */
      const onClick = (e: MouseEvent) => {
        if (!e) return;
        e.stopPropagation(); // prevent events fired by parent elements that also listen on click

        let target = e.target as HTMLElement;

        /** Similar to {@link MenuItem.el} */
        let possibleMenuItemEl: HTMLElement | undefined;

        /** make sure we hit an element, that is related to a {@link MenuItem} */
        while (!possibleMenuItemEl && target && target.parentElement) {
          if (target.getAttribute('role') !== 'menuitem') {
            target = target.parentElement;
          } else {
            possibleMenuItemEl = target;
          }
        }

        // no match found
        if (!target) return;

        /** possible {@link MenuItem} is not a `HTMLLIElement` (only `HTMLLIElement`s should trigger {@link MenuItem.goToNextLevel} or {@link MenuItem.goToNextItem}) */
        if (!possibleMenuItemEl || possibleMenuItemEl.nodeName !== 'LI') return;

        /** Find the {@link MenuItem}, that belongs to the clicked target */
        const theMenuItem = this.menuItems.find(
          (el) => el.el === possibleMenuItemEl,
        );
        if (!theMenuItem) return;

        if (theMenuItem.subMenuItem) {
          if (theMenuItem.el.getAttribute('aria-expanded') !== 'true') {
            const directionConfig = findDirectionConfig(menuItem);

            if (directionConfig?.closeOnNavigate !== false) {
              /** Open {@link MenuItem}, close all already opened submenus and and go to next level */
              resetAllInLevel(theMenuItem.level);
            }
            theMenuItem.goToNextLevel();
          } else {
            /** Close {@link MenuItem} */
            theMenuItem.el.setAttribute('aria-expanded', 'false');
          }
        }
      };

      /**
       * Controls for {@link MenuItem}
       */
      const menuItemControls = (e: KeyboardEvent) => {
        if (!e) return;
        if (e.repeat) return;

        const directionConfig = findDirectionConfig(menuItem);
        const direction = directionConfig?.direction || this.defaultDirection;

        // maintain the functionality of shortcuts and key-combinations by ignoring ctrl, shift, meta (Win/cmd) and alt keys
        if (e.ctrlKey || e.shiftKey || e.metaKey || e.altKey) return;
        if (!menuItem.current) return;

        switch (e.code) {
          case 'ArrowLeft':
            e.preventDefault();
            if (direction === 'horizontal') {
              menuItem.goToPreviousItem();
            } else {
              if (menuItem.level === 1) {
                menuItem.goToPreviousLevel();
                menuItem.parentMenuItem?.goToPreviousItem();
              } else {
                if (findDirectionConfig(menuItem)?.closeOnNavigate !== false) {
                  resetAllInLevel(menuItem.level - 1);
                }
                menuItem.goToPreviousLevel();
              }
            }
            break;

          case 'ArrowRight':
            e.preventDefault();
            if (direction === 'horizontal') {
              menuItem.goToNextItem();
            } else {
              if (!menuItem.goToNextLevel()) {
                if (menuItem.level === 1) {
                  menuItem.goToPreviousLevel();
                  menuItem.parentMenuItem?.goToNextItem();
                } else {
                  menuItem.goToNextRootItem();
                }
              }
            }
            break;

          case 'ArrowUp':
            e.preventDefault();
            if (direction === 'vertical') {
              if (!menuItem.isFirst) {
                menuItem.goToPreviousItem();
              }
            } else {
              menuItem.goToPreviousLevel();
            }
            break;

          case 'ArrowDown':
            e.preventDefault();
            if (direction === 'vertical') {
              menuItem.goToNextItem();
            } else {
              menuItem.goToNextLevel();
            }
            break;

          case 'Space':
          case 'Enter':
            if (menuItem.el.nodeName === 'LI') {
              menuItem.goToNextLevel();
            }
            break;

          case 'Escape':
            e.preventDefault();

            resetAllInLevel(menuItem.level - 1);
            menuItem.goToPreviousLevel();
            break;

          default:
            break;
        }
      };

      if (menuItem.el.nodeName === 'LI') {
        menuItem.el.addEventListener('click', onClick as EventListener);
      }

      /** Click on a {@link MenuItem} with a hyper link reference (link) */
      const onAnchorLinkClick = () => {
        if (findDirectionConfig(menuItem)?.closeOnClick === true)
          resetAllInLevel();
      };

      if (menuItem.el.nodeName === 'A') {
        menuItem.el.addEventListener('click', onAnchorLinkClick);
      }

      /**
       * Check the status of any {@link MenuItem} and apply attributes and styles
       */
      const updateMenuItem = (el: MenuItem) => {
        /** Check if {@link MenuItem} is the current {@link MenuItem} */
        if (el.current) {
          el.el.dataset.focus = 'true';
          document.addEventListener('keydown', menuItemControls);
        } else {
          el.el.dataset.focus = 'false';
          document.removeEventListener('keydown', menuItemControls);
        }
      };

      const onMenuItemFocus = () => {
        this.menuItems
          ?.filter((el) => el.current === true)
          .map((el) => {
            el.current = false;
            updateMenuItem(el);
          });
        menuItem.current = true;
        updateMenuItem(menuItem);
      };

      menuItem.el.addEventListener('focus', onMenuItemFocus);
      menuItem.el.addEventListener('focusout', () => {
        document.removeEventListener('keydown', menuItemControls);
      });
    };

    /**
     * Set attributes on each {@link MenuItem} (requires each {@link MenuItem} to be complete)
     */
    const setRelativeAttributes = () => {
      /** Now each {@link MenuItem} is complete and we can apply styles etc. relative to the {@link MenuItem}s data */
      this.menuItems.forEach((menuItem, i) => {
        menuItem.el.tabIndex = i === 0 ? 0 : -1;
        menuItem.el.dataset.level = menuItem.level.toString();
        menuItem.el.setAttribute('role', 'menuitem');
        this.handleCurrentPage(window.location.href, menuItem);
        if (menuItem.subMenuItem)
          menuItem.el.setAttribute('aria-expanded', 'false');
        bindEventListeners(menuItem);
      });
    };

    setDefaultAttributes();
    findMenuItemParameters();
    setRelativeAttributes();
    onClickOutside(this.theMenu, resetAllInLevel);

    /** Trigger scroll events */
    this.OnScrollCloseAll();

    return true;
  }
}
