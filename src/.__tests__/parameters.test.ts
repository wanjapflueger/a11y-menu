import { Menu } from '../index';

const myMenu = new Menu();
const nav = document.createElement('nav');
const notANavElement = document.createElement('div');

test('PARAM → "nav" not having nodeName "NAV" will be rejected', () => {
  expect.assertions(1);
  try {
    myMenu.Create({
      nav: notANavElement,
    });
  } catch (error) {
    expect(error).toBeInstanceOf(TypeError);
  }
});

test('PARAM → "nav" having nodeName "NAV"', () => {
  expect(
    myMenu.Create({
      nav,
    }),
  ).toBe(true);
});

test('PARAM → "config.responsive"', () => {
  expect(
    myMenu.Create({
      nav,
      config: {
        breakpoints: [
          {
            width: 1280,
          },
        ],
      },
    }),
  ).toBe(true);
});

test('PARAM → "config.responsive[].width cannot be < 0"', () => {
  expect(
    myMenu.Create({
      nav,
      config: {
        breakpoints: [
          {
            width: -300,
          },
        ],
      },
    }),
  ).toBe(false);
});
