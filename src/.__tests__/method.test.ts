import { Menu } from '../index';

const myMenu = new Menu();
const nav = document.createElement('nav');

test('METHOD → Create() creates a menu', () => {
  expect(
    myMenu.Create({
      nav,
    }),
  ).toBe(true);
});
