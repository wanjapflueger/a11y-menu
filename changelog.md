# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v3.0.0] - 2023-09-07

### Change

- Bumped major version

## [v2.6.1] - 2023-06-19

### Fixed

- In some complex menu structures the parent menu item was not identified correctly in `findParentMenuItem`

## [v2.6.0] - 2023-03-30

🚨 BREAKING: Use [v2.5.2] if this does not work for you.

- A menu item must no longer contain a direct child `HTMLSpanElement` and `HTMLUListElement`. Instead we use the the first `HTMLUListElement` as the submenu and the first `HTMLSpanElement` as label for the submenu.

### Changed

## [v2.5.2] - 2023-02-03

- Version [v2.5.2] will be the last version that is working for most project. Please use [https://gitlab.com/barrierefrei/a11y-menu-2](https://gitlab.com/barrierefrei/a11y-menu-2) in the future. Version [v3.0.0] was only released to fix a specific problem for a specific client and should not be used elsewhere!
- Fix a comparison operator

## [v2.5.1] - 2023-02-03

- Make method `CloseAll` public

### Added

## [v2.5.0] - 2023-02-02

- All currently openened `MenuItem`s will be closed on scroll (see `OnScrollCloseAll`).

## [v2.4.0] - 2023-01-30

### Removed

- Remove unused `get` methods to reduce package size

### Changed

- Using Node 18
- Keydown events will not fire on repeat. This makes the menu more robust.
- Refactored some parts of the code basis to improve readability and developing experience

## [v2.3.0] - 2022-11-25

### Changed

- Upgrade node version from `12` to `16`

### Fixed

- Only menu items that are anchor links (`HTMLAnchorElement`) can have `aria-current="page"` (before also `HTMLLiElement` could be targeted)

## [v2.2.0] - 2022-11-11

### Added

- All menu items that are **parents** of a menu item that has an attribute `aria-current="page"`, will also have an attribute `data-current-parent="true"` (resolves https://gitlab.com/wanjapflueger/a11y-menu/-/issues/3)

### Changed

- Updated readme

## [v2.1.1] - 2022-11-04

### Changed

- When trying to set `aria-current="page"` we also include URLs like `//domain.com`. So we can handle in total:
  - `http://domain.com`
  - `https://domain.com`
  - `https://domain.com/foo`
  - `//domain.com/foo`
  - `/foo`
  - Trailing slashes will be ignored so that `/foo/` equals `/foo`
  - Hashes will be ignored so `/foo/#bar` equals `/foo`

## [v2.1.0] - 2022-11-04

### Added

- Try to add `aria-current="page"` to any `[role="menuitem"]` that matches the current URL in the browsers address bar

## [v2.0.0] - 2022-08-19

### Added

- `closeOnClick` option (see version changes for more information)
- A menu item will close, when opening another menu item with the same or a lower level. This behaviour was standard up to [v1.2.3]. You can now decide to keep the menu item open with the new `closeOnNavigate` opion.

### Changed

- [💥 BREAKING] Rename parameters and possible values to make it easier to understand
- Improved some documentation
- Clicking on a menu item with a hyper link reference (link) would always close all menu items. This is no longer the case. The currently opened menu items will remain open on click. You may choose to change this behaviour for a certain breakpoint and menu item level with the `closeOnClick` option

## [v1.2.3] - 2022-06-03

### Fixed

- Clicking outside a navigation should close it. This was not the case when the clicked element was also a navigation.

## [v1.2.0] - 2022-05-31

### Fixed

- An error when requesting the next menu item within a submenu, the focus would jump to the next menu item with a higher index value and the same level – although the menu item in focus is not part of the same submenu structure.

### Changed

- Updated examle link
- Simplify instructions in README

## [v1.1.0] - 2021-12-30

### Fixed

- [Submenu entries not being closed automatically, when opening another](https://gitlab.com/wanjapflueger/a11y-menu/-/issues/1)

## [v1.0.0] - 2021-11-14

### Changed

- Readme

### Added

- Tested with various screen readers

## [v0.0.1] - 2021-10-29

### Added

- Initial release

[unreleased]: https://gitlab.com/wanjapflueger/a11y-menu
[v3.0.0]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v2.6.1...v3.0.0
[v2.6.1]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v2.6.0...v2.6.1
[v2.6.0]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v2.5.1...v2.6.0
[v2.5.1]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v2.5.0...v2.5.1
[v2.5.0]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v2.4.0...v2.5.0
[v2.4.0]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v2.3.0...v2.4.0
[v2.3.0]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v2.2.0...v2.3.0
[v2.2.0]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v2.1.1...v2.2.0
[v2.1.1]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v2.1.0...v2.1.1
[v2.1.0]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v2.0.0...v2.1.0
[v2.0.0]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v1.2.3...v2.0.0
[v1.2.3]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v1.2.0...v1.2.3
[v1.2.0]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v1.1.0...v1.2.0
[v1.1.0]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v1.0.0...v1.1.0
[v1.0.0]: https://gitlab.com/wanjapflueger/a11y-menu/-/compare/v0.0.1...v1.0.0
[v0.0.1]: https://gitlab.com/wanjapflueger/a11y-menu/-/tree/v0.0.1
